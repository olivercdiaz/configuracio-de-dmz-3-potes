# Configuració de DMZ 3 potes

_`HE USADO OTRAS IP'S, ya que me parecía más lógico no usar las mismas IP's que TODA la clase.`_

Para empezar vamos a instalar y a configurar las máquinas virtuales necesarias, con las interfaces correctas.

1. Máquina Ubuntu desktop

- Red interna (LAN)

![imagen](imagen/1.png)

2. pfsense

- Adaptador Puente (Conexión a internet)
- Red Interna (LAN)
- Red Interna (DMZ)

![imagen](imagen/2.png)
![imagen](imagen/3.png)
![imagen](imagen/4.png)

3. Ubuntu server

- Red Interna (DMZ)

![imagen](imagen/5.png)

Ahora vamos a configurar las redes en el pfsense.

1. Primero ordenamos las redes.

![imagen](imagen/6.png)

2. Aquí podemos ver las IP's otorgadas para el WAN y el LAN. (Adaptador puente) y (Red Interna)

![imagen](imagen/7.png)

3. Ahora al configurar las IP's ya tenemos la IP.

![imagen](imagen/8.png)

4. Como anteriormente configuramos las máquinas virtuales, podremos ir al Ubuntu Deskto y probar a ejecutar la IP en el navegador firefox de ubuntu desktop.

![imagen](imagen/9.png)

Usuario: admin
Contraseña: pfsense

--> Es la que viene por defecto.

5. Procedemos a configurar el pfsense

![imagen](imagen/10.png)

La siguiente ventana no la tocamos, en la última solo tenemos que desmarcar la siguiente casilla

![imagen](imagen/11.png)

Ahora nos pondrá nuestra IP

![imagen](imagen/12.png)

Ponemos una Contraseña

![imagen](imagen/13.png)

Un reload, y se guardan los cambios

![imagen](imagen/14.png)

Ahora procedemos a poner una tercera IP estática, esto será para usar la red que anteriormente pusimos como DMZ.

![imagen](imagen/15.png)
![imagen](imagen/16.png)

Vamos al pfsense y comprobamos que efecticamente la IP se ha puesto correctamente en la opción vacia OPT1.

![imagen](imagen/17.png)

Configuramos el DNS para poder hacer ping y tener internet.

![imagen](imagen/18.png)

![imagen](imagen/19.png)

Ahora configuramos el tráfico SSH.

1. Vamos a ir a Floating y add

2. Vamos a Interface y Source y ponemos --> LAN

3. En Destination --> SSH (22)

![imagen](imagen/20.png)

Comprobamos que podemos hacer un SSH

![imagen](imagen/21.png)

Hago un nmap del DMZ para ver que puertos me muestra.

![imagen](imagen/22.png)

Para que no deje hacer ping desde LAN, crearé una ruta que deniege desde LAN a cualquier equipo externo

![imagen](imagen/23.png)

![imagen](imagen/24.png)
